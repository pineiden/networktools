#!/usr/bin/env python

"""
Test asyncio generator withn Queue
Create 2 coroutines, one sends the items to the other

"""

import asyncio
from networktools.library import my_random_string
from networktools.queue import send_async_queue, read_async_queue
from networktools.colorprint import rprint, gprint


async def creation_chars(queue, number):
    """
    Ask a value and create a list of random strings

    """
    count = 0
    while count < number:
        new_str = my_random_string()
        print("Source str->", new_str, "<")
        await send_async_queue(queue, new_str)
        await asyncio.sleep(.2)
        count += 1
    await queue.put("END")


async def lower_chars(queue):
    """
    Read from queue the items and put in lowercase

    """
    gprint("Running upper_chars")
    new_str = ""
    while new_str != "END":
        async for new_str in read_async_queue(queue, 'upper_chars'):
            gprint("=RECV NEW STR=")
            print("|!", new_str, type(new_str), "!|")
            rprint(new_str.strip().lower())


if __name__ == "__main__":
    VALUE = int(input("Dame un valor\n"))
    LOOP = asyncio.get_event_loop()
    A_QUEUE = asyncio.Queue()
    T0 = LOOP.create_task(creation_chars(A_QUEUE, VALUE))
    T1 = LOOP.create_task(lower_chars(A_QUEUE))
    TASKS = [T0, T1]
    LOOP.run_until_complete(asyncio.gather(*TASKS))
