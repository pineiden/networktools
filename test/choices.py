from networktools.library import choice_input


if __name__ == "__main__":
    commands = {0: "PRINT",
                1: "AUTH_MSG",
                2: "ADD_SOCK",
                3: "ACTIVE_SOCK",
                4: 'CONNECT_SOCK',
                5: "SEND_MSG"}
    opt = lambda x: 'LOW' if x in [0,1,2] else 'HIGH'
    command, key, msg_type = choice_input(commands, type_opt_lambda=opt)

    print("====="*5)
    print(command, key, msg_type)
